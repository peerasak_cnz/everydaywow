//
//  ProductTableViewCell.swift
//  EverydayWow
//
//  Created by Peerasak Unsakon on 12/3/2558 BE.
//  Copyright © 2558 Peerasak Unsakon. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productPercentOff: UILabel!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productNormalPriceLabel: UILabel!
    @IBOutlet weak var productSpecialPriceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
