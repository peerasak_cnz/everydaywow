//
//  ProductTableViewController.swift
//  EverydayWow
//
//  Created by Peerasak Unsakon on 12/3/2558 BE.
//  Copyright © 2558 Peerasak Unsakon. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
import JHSpinner
import SafariServices

class ProductTableViewController: UITableViewController, SFSafariViewControllerDelegate {

    var productArray: Array<NSDictionary>? = []
    var currentPage = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.getPage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getPage() {
        
        let spinner = JHSpinnerView.showOnView(view, spinnerColor:UIColor.redColor(), overlay:.FullScreen, overlayColor:UIColor.whiteColor().colorWithAlphaComponent(0.8))
        view.addSubview(spinner)
        
        let header = [
            "Accept": "application/json, text/javascript, */*; q=0.01",
            "X-Requested-With": "XMLHttpRequest"
        ]
        
        let pathURL = "https://www.itruemart.com/everyday-wow-all"
        let params = [
            "page": "\(self.currentPage)",
            "orderby": "desc",
            "sortby": "published_at"
        ]
        
        self.title = "Page: \(self.currentPage)"
        
        self.productArray = []
        
        Alamofire.request(.GET, pathURL, parameters: params, headers: header).responseJSON { response in
            
            spinner.dismiss()
            
            if let statusCode = response.response?.statusCode {
                if (statusCode == 200) {
                    let json = JSON(data: response.data!)
                    
                    for item in json["data"]["product_data"].arrayObject! {
                        self.productArray?.append(item as! NSDictionary)
                    }
                    
                    self.tableView.contentOffset = CGPointMake(0, 0 - self.tableView.contentInset.top);
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.productArray?.count)!
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        
        let product = self.productArray![indexPath.row] 
        
        let cell = tableView.dequeueReusableCellWithIdentifier("ProductTableViewCell", forIndexPath: indexPath) as! ProductTableViewCell
        
        cell.productImageView.image = nil
        
        Alamofire.request(.GET, "https:\(product["mobile_image"]!)")
            .responseImage { response in
                if let image = response.result.value {
                    cell.productImageView.image = image
                }
        }

        cell.productPercentOff.text = "\(product["percent_discount"]!)%"
        cell.productTitleLabel.text = "\(product["product_title"]!)"
        cell.productNormalPriceLabel.text = "\(product["normal_price"]!) THB"
        cell.productSpecialPriceLabel.text = "\(product["special_price"]!) THB"

        
        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        let product = self.productArray![indexPath.row]
        
        let url = NSURL(string: "\(product["product_url"]!)")!
        let safariVC = SFSafariViewController(URL: url)
        safariVC.delegate = self
        self.navigationController?.pushViewController(safariVC, animated: true)
    }
    
    @IBAction func nextPageAction(sender: AnyObject) {
        self.currentPage = self.currentPage + 1
        self.getPage()
    }
    
    @IBAction func previousPageAction(sender: AnyObject) {
        if self.currentPage >= 2 {
            self.currentPage = self.currentPage - 1
            self.getPage()
        }
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
